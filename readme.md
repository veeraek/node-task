# *Node Task*

*Node Task with Markdown*

## *Installations steps*

1. Copy the repository
2. Install dependencies
3. Start the app
   
## *Tekijä:*
```sh
"firstName": "Veera",
"lastName": "Koivisto",
"age": 26
git clone https://gitlab.com/veeraek/node-task.git
cd ./node-task
npm i
}
```
Start cmd: `npm run start`

### *Hello* 
![alt text for test image](kesa.jpg)

## *Done*
- [ ] Ignore
- [ ] Markdown
- [ ] Functions